package pl.sda.springtraining.spring.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationDAO {
    private UserRepository userRepository;

    @Autowired// constructor injection
    public UserRegistrationDAO(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveNewUser(User user) {
        if (userExists(user.getEmail())) {
            throw new UserExitsException("Użytkoniek o takim emailu " + user.getEmail() + "  istnieje");
        }
        userRepository.save(user);
    }

    public boolean userExists(String email) {
        if (userRepository.findUserByEmail(email)!= null){
            return true;
        }
        return false;
    }
}
