package pl.sda.springtraining.spring.user;

public class UserExitsException extends RuntimeException {

    public UserExitsException(String mesage) {
        super(mesage);
    }
}
