package pl.sda.springtraining.spring.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductDAO productDAO;
    private final productToProductDTOBuilder productToProductDTOBuilder;

    @Autowired
    public ProductService(productToProductDTOBuilder productToProductDTOBuilder) {
        this.productToProductDTOBuilder = productToProductDTOBuilder;
    }

    public ProductDTO createNewProduct(String productName, Integer stockAmount, BigDecimal price, ProductType productType) {
        Product product = new Product();
        product.setProductName(productName);
        product.setStockAmount(stockAmount);
        product.setPrice(price);
        product.setProductType(productType);
        return productToProductDTOBuilder.rewriteProductToProductDTO(productDAO.saveProduct(product));
    }

    public List<ProductDTO> findProduct(String query, String productType) {
        return productDAO.findProducts(query,productType).stream()
                .map(p -> productToProductDTOBuilder.rewriteProductToProductDTO(p))
                .collect(Collectors.toList());
    }

    public Optional<Product> findProductById(Integer id) {
        return productDAO.findProductById(id);
    }

    public void updateProduct(Product product) {// todo zmienic produkt na DTO
        Product productById = productDAO.findProductById(product.getId()).get();
        productById.setProductName(product.getProductName());
        productById.setStockAmount(product.getStockAmount());
        productById.setPrice(product.getPrice());
        productDAO.saveProduct(productById);
    }
}
