package pl.sda.springtraining.spring.product;

import lombok.Getter;
import lombok.Setter;
import pl.sda.springtraining.spring.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Product extends BaseEntity implements ProductInfoHolder {
    //    @Id
//    @GeneratedValue
//    private Integer id;
    private String productName;
    private Integer stockAmount;
    private BigDecimal price;
    @Enumerated(EnumType.STRING)
    private ProductType productType;
}
