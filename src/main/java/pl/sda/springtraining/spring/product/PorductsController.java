package pl.sda.springtraining.spring.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class PorductsController {

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/products")
    public String showProduct(@RequestParam(required = false) String query, @RequestParam(required = false) String productType, Model model) {
        model.addAttribute("productsList", productService.findProduct(query, productType));
        model.addAttribute("queryValue", query);
        model.addAttribute("selectedProductType", productType);
        model.addAttribute("productTypes", ProductType.values());
        return "products";
    }
}
